module archanan-login-service

go 1.14

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/mock v1.3.1
	github.com/lestrrat-go/jwx v1.0.4
	github.com/sebdah/goldie/v2 v2.5.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.5.1
	go.uber.org/fx v1.13.1
)
