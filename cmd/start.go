package cmd

import (
	"context"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.uber.org/fx"
)

type Param struct {
	fx.In

	G         *gin.Engine
	Lifecycle fx.Lifecycle
}

func InitializeService(p Param) {
	p.Lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				port := os.Getenv("PORT")
				logrus.Infof("[START SERVER] service run on port: %s", port)
				go startServer(p.G, port)
				return nil
			},

			OnStop: func(context.Context) error {
				logrus.Info("[CLOSE SERVER] Done stop server")
				return nil
			},
		},
	)
}

func startServer(handler http.Handler, port string) {
	srv := http.Server{
		Addr:    ":" + port,
		Handler: handler,
	}

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logrus.WithFields(logrus.Fields{"error": err}).
			Errorf("[START ERROR] listening port service error")
	}
}
