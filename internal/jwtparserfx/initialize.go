package jwtparserfx

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"

	"archanan-login-service/pkg/jwtparser"
)

var Initialize = fx.Provide(provideJWTParser)

func provideJWTParser() jwtparser.Parser {
	jwtKeyURL := viper.GetString("aws.jwks")
	if len(jwtKeyURL) == 0 {
		logrus.Panic("config aws jwks url is empty. Should add this config to env, configs file.")
	}

	parser, err := jwtparser.New(jwtKeyURL)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).
			Panicf("fail to init parser JWT with error")
	}
	return parser
}
