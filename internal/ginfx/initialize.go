package ginfx

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"
)

var Initialize = fx.Provide(initializeGin)

func initializeGin() *gin.Engine {
	logrus.Infof("gin mode - debug: %t", viper.GetBool("api.debug"))
	if viper.GetBool("api.debug") {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	return gin.New()
}
