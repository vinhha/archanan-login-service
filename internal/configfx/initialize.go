package configfx

import (
	logger "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.uber.org/fx"

	"archanan-login-service/pkg/configutils"
)

func Initialize(svcName string, cfgFile, cfgPath string) fx.Option {
	return fx.Invoke(func() {
		configutils.LoadConfiguration(svcName, cfgFile, cfgPath)
		logger.Infof("Starting %s\n", viper.GetString("title"))
	})
}
