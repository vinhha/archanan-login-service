package servicefx

import "go.uber.org/fx"

var Initialize = fx.Options(
	fx.Provide(provideServiceRepository),
	fx.Invoke(registerService),
)
