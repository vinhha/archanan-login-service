package servicefx

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/fx"

	"archanan-login-service/pkg/jwtparser"
	"archanan-login-service/pkg/router"
)

type params struct {
	fx.In

	GinRouter  *gin.Engine
	Repository router.Repository
}

func registerService(p params) {
	p.GinRouter.Use(corsMiddleware())
	baseRouter := router.NewRouter(p.Repository)
	baseRouter.Register(p.GinRouter)
}

func provideServiceRepository(parser jwtparser.Parser) router.Repository {
	return router.NewRepository(parser)
}

/*
	Func: Help allow all request from other domain, to skip error with CORS
*/
func corsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding,"+
			"X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}

		c.Next()
	}
}
