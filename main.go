package main

import (
	"go.uber.org/fx"

	"archanan-login-service/cmd"
	"archanan-login-service/internal/configfx"
	"archanan-login-service/internal/ginfx"
	"archanan-login-service/internal/jwtparserfx"
	"archanan-login-service/internal/logfx"
	"archanan-login-service/internal/servicefx"
)

func main() {
	app := fx.New(
		configfx.Initialize("login-service", "config", "configs"),
		ginfx.Initialize,
		logfx.Initialize,
		jwtparserfx.Initialize,
		servicefx.Initialize,
		fx.Invoke(cmd.InitializeService),
	)
	app.Run()
}
