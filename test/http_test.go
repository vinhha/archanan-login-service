package test

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"

	"github.com/bitly/go-simplejson"
	"github.com/gin-gonic/gin"
	logger "github.com/sirupsen/logrus"
)

func performHTTPRequest(r http.Handler, method, path string, headers map[string]string,
	params map[string]string, body io.Reader,
) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, body)

	if len(headers) != 0 {
		for key, value := range headers {
			req.Header.Add(key, value)
		}
	}

	if len(params) != 0 {
		q := req.URL.Query()
		for k, v := range params {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	return w
}

func buildHeaderRequest(contentType string) map[string]string {
	headerMap := make(map[string]string)
	headerMap["Content-Type"] = contentType
	return headerMap
}

func prepareBodyReader(obj interface{}, contentType string) (io.Reader, error) {
	jsonReq, _ := json.Marshal(obj)
	var bodyReader io.Reader
	switch contentType {
	case gin.MIMEJSON:
		bodyReader = strings.NewReader(string(jsonReq))
	case gin.MIMEPOSTForm:
		jsonBody, err := simplejson.NewFromReader(strings.NewReader(string(jsonReq)))
		if err != nil {
			return nil, err
		}

		values := url.Values{}
		bodyMap, err := jsonBody.Map()
		if err != nil {
			return nil, err
		}

		for key, value := range bodyMap {
			logger.Debug()
			values.Set(key, fmt.Sprintf("%v", value))
		}
		bodyReader = strings.NewReader(values.Encode())
	}

	return bodyReader, nil
}
