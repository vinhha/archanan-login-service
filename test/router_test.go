package test

import (
	"net/http"
	"testing"

	"github.com/bitly/go-simplejson"
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/sebdah/goldie/v2"
	"github.com/stretchr/testify/suite"

	"archanan-login-service/pkg/errors"
	"archanan-login-service/pkg/router"
	mockrouter "archanan-login-service/pkg/router/gomock"
)

type routerSuite struct {
	suite.Suite
	goMockCtrl *gomock.Controller

	ginEngine *gin.Engine

	service *router.Service
	repo    *mockrouter.MockRepository
}

func TestRepoSuite(t *testing.T) {
	suite.Run(t, new(routerSuite))
}

func (s *routerSuite) SetupTest() {
	s.ginEngine = gin.New()
	s.goMockCtrl = gomock.NewController(s.T())
	s.repo = mockrouter.NewMockRepository(s.goMockCtrl)
	s.service = router.NewRouter(s.repo)
}

func (s *routerSuite) TearDownTest() {
	s.goMockCtrl.Finish()
}

// ------------------------------------------------------------- //
// TEST: Test router with /healthcheck
// ------------------------------------------------------------- //

/*
	Test case 1: Call API healthcheck
	Expect: return response success
*/
func (s *routerSuite) TestCase1_CallVerifyHealthCheck() {
	s.service.Register(s.ginEngine)

	w := performHTTPRequest(s.ginEngine, "GET", "/healthcheck", nil, nil, nil)

	s.Equal(http.StatusOK, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

// ------------------------------------------------------------- //
// TEST: Test router with /internal
// ------------------------------------------------------------- //

/*
	Test case 1: Authorization not found in header
	Expect: Should return error with http code Bad Request - 400
*/
func (s *routerSuite) TestCase1_AuthorizationKeyNotFound() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusBadRequest, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

/*
	Test case 2: Authorization found in header, but len of header is not valid
	Expect: Should return error with http code Bad Request - 400
*/
func (s *routerSuite) TestCase2_AuthorizationKeyInvalid() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	header["Authorization"] = "Bearer"
	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusBadRequest, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

/*
	Test case 3: Authorization found in header, but token parse error
	Expect: Should return error with http code StatusUnauthorized - 401
*/
func (s *routerSuite) TestCase3_AuthorizationWithTokenIsInvalid() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	token := "flkfjklmncvkajfdksmjnclksajnmflksdjml"
	header["Authorization"] = "Bearer " + token

	s.repo.EXPECT().
		AuthenticateTokenHandler(gomock.Any(), token).
		Return(false, errors.ErrParseTokenError)

	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusUnauthorized, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

/*
	Test case 4: Authorization found in header, but token is expired
	Expect: Should return error with http code StatusUnauthorized - 401
*/
func (s *routerSuite) TestCase3_AuthorizationWithTokenIsIExpired() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	token := "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA"
	header["Authorization"] = "Bearer " + token

	s.repo.EXPECT().
		AuthenticateTokenHandler(gomock.Any(), token).
		Return(false, errors.ErrTokenIsExpired)

	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusUnauthorized, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

/*
	Test case 5: Authorization found in header, but token verify successfully
	Expect: Should return error with http code StatusUnauthorized - 401
*/
func (s *routerSuite) TestCase5_AuthorizationWithTokenSuccess() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	token := "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA"
	header["Authorization"] = "Bearer " + token

	s.repo.EXPECT().
		AuthenticateTokenHandler(gomock.Any(), token).
		Return(true, nil)

	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusOK, w.Code)
	resJSON, err := simplejson.NewFromReader(w.Body)
	s.NoError(err)
	encoded, err := resJSON.EncodePretty()
	s.NoError(err)
	gd := goldie.New(s.T())
	gd.Assert(s.T(), s.T().Name(), encoded)
}

/*
	Test case 6: Authorization found in header, but token is invalid
	Expect: Should return error with http code StatusUnauthorized - 401
*/
func (s *routerSuite) TestCase6_AuthorizationWithTokenIsInvalid() {
	s.service.Register(s.ginEngine)

	body, _ := prepareBodyReader(nil, gin.MIMEPOSTForm)
	header := buildHeaderRequest(gin.MIMEPOSTForm)
	token := "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA"
	header["Authorization"] = "Bearer " + token

	s.repo.EXPECT().
		AuthenticateTokenHandler(gomock.Any(), token).
		Return(false, nil)

	w := performHTTPRequest(s.ginEngine, "POST", "/internal", header, nil, body)

	s.Equal(http.StatusUnauthorized, w.Code)
}
