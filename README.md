# Archanan - Login Service

## Table of content

1. [Introduction](#introduction)
2. [Software Requirement Analysis](#requirement_analysis)
3. [Solution Implement & Data Flow Diagram](#solution)
4. [How to run](#how_to_run)
5. [Report & Review](#report_review)
6. [Demo](#demo)
7. [ToDo](#todo)


## Archanan - Login Service <a name="introduction"></a>

Write an application which demonstrates the use of ​`Amazon Cognito`​ for managing authentication of access to an HTTP server.
Additionally, provide a written description of how would the application integrate with the front end (document integration).

## Software Requirement Analysis <a name="requirement_analysis"></a>
- Requirement: [spec_requirement](./requirement/archanan_assignment_3.pdf)

#### Task 1: An API implemented as an HTTP server written in Go with 2 public API (detail in spec requirement)
- With this task, developer need develop a service with REST API, expose 2 public API: 

| Method | Path | Request | Response | Note | 
|:---:|:---|:---|---| --- | 
| GET| /healthcheck | x | - Content-Type: `application/json`<br>- Sample response: `{"status":"healthy"}`<br> - HTTP Code: `200` | | 
| POST | /internal | - Header: `Authorization: Bearer <Token>` | - Content-Type: `application/json` <br>- HTTP Code: `200` if valid JWT Token<br> - HTTP Code: `401` if invalid JWT Token

- With API `/internal` must to decode JWT Token from client, check valid token from our User Pool at AWS Cognitor (ex with
info of User Pool: PoolID, ClientID, JWKS -  `JSON Web Key Set`, ...)

#### Task 2: A small JavaScript web application
- Write a small web application with `simple html/css`, show a view which help user can fill our info (User Name/ Password) to
login and authenticate before access our service. 
- Call from web application to AWS Cognito to verify User in User Pool, then AWS Cognito return JWT Token. 
Web App call to Service above, to authenticate and render content, http code from response of API. 
  
## Solution Implement & Data Flow Diagram <a name="solution"></a>

#### Over view flow diagram:

![Flow Diagram](./docs/diagram/over_view.png)

#### Task 1: An API implemented as an HTTP server written in Go with 2 public API (detail in spec requirement)
- Implement main Service with name `LoginService` with web framework `Gin-Gonic` to route 2 router: 
    ```go
      router.GET("/healthcheck")
      router.POST("/internal")
    ```

- Handle business logic: authenticate with token from request.
    + If **valid JWT token**, return sample content with **JSON-encoded** and HTTP Code is **200** (`http.StatusOK`). Such as:
    ```json
      {
         "return_code":1,
         "return_message":"login successfully",
         "detail_content":"Welcome to Archanan Company"
      }
    ```
    + If `invalid JWT token`, return HTTP Code is **401** (`http.StatusUnauthorized`) and response body with type **JSON-encoded**
    (_if necessary_). 
    ```json
      {
         "return_code":-1,
         "return_message":"TOKEN_IS_INVALID"
      }
    ```
    
- Implement `Parser` package to decode JWT Token string from Client. And verify token with sign, payload is vaid a Token
from our User Pool at AWS Cognito. 
    + Ref at [link](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-verifying-a-jwt.html#amazon-cognito-user-pools-using-tokens-step-1_) -
 document of AWS to help verify token.
    + Tech ref at [link](https://github.com/awslabs/aws-support-tools/tree/master/Cognito/decode-verify-jwt)

- Unit test with 2 router and business logic (_mock test if necessary_)

#### Task 2: A small JavaScript web application

- Write a simple file html/css to render a page Login, help user can fill UserName (`email`) and Password to login to Archanan System. 
- Call to Cognito to verify User. If exist User in User Pool, AWS Cognitor will callback **a JWT Token** to Web Application.
- Call to `LoginService` to authenticate **a JWT Token**. Then, web application receive response from API, render content and HTTP Code on page. 

## TODO <a name="todo"></a>
- [x] Analyst Requirement
- [x] Find solution, and describe by text
- [x] Write spec API Integration (_ex: Swagger, ..._)
- [x] Research about AWS Cognito to create a User Pool, authenticate a JWT Token is valid
- [x] Research about AWS Amplify to deploy a simple Web Application
- [x] Implement `Parser` package to parse and verify a JWT Token from AWS Cognito expose. 
- [x] Implement `LoginService` to handle 2 router. 
- [x] Unit test/mock test all project, should coverage to 100% 
- [x] Write report and review
- [x] Integrate with AWS Cognito or deploy demo on personal VPS (`GCP - Linux`) (_if necessary_)

## How to run <a name="how_to_run"></a>
### With Web Service: 
**Ref Project**: https://gitlab.com/vinhha/archanan-login-service
- **Solution 1**: Point at root project, run comment `go run main.go` to compile and run service. Service will read config from file `./configs/config.yml`
and expose with port `8080`
    + Collection POSTMAN API: [Link collection](https://www.getpostman.com/collections/20d891e17773168de161)
- **Solution 2**: In folder `./deployment`, you can run file `docker-compose` ([file](./deployment/docker-compose.yml)) with command `docker-compose up --build` to
compile, and run service base on Docker Image and container. 
- **Solution 3**: Or other way, you can test by test API on service demo, detail at [Demo](#demo)
    + Service has been deployed on VPS (_GCP, Google Cloud - Linux (1 CPU, 4Gb RAM)_) at address: http://34.71.225.33:8080
    + Service has been deployed on Heroku: https://tranquil-scrubland-23383.herokuapp.com
    
### With Web Application: 
Ref Project: https://gitlab.com/vinhha/archanan-web-application
- **Solution 1**: Point at root project, run comment `yarn start` to compile and run service. 
- **Solution 2**: Or other way, you can test by test web application on demo at address https://master.d1wo0to0xi8n6u.amplifyapp.com.
It has been deploy on Amplify AWS via trigger with Gitlab Project, on branch master. 

## Report & Review <a name="report_review"></a>
### Summary: 
#### Synthesize documents and links:

| Title | URL | Description|
|---|---|---|
| Web Service Repo| https://gitlab.com/vinhha/archanan-login-service | Resository to implement back end service (Golang) |
| Web Application Repo| https://gitlab.com/vinhha/archanan-web-application | Resository to implement web application (ReactJS) |
| Web Service Demo on VPS | http://34.71.225.33:8080 | This is only base URL (host) |
| Web Service Demo on Heroku | https://tranquil-scrubland-23383.herokuapp.com | This is only base URL (host) |
| Web Application on Amplify | https://master.d1wo0to0xi8n6u.amplifyapp.com/ | Support Login, show response verify JWT and logout |

#### What did I learned:
- Through this test assignment, I researched and learned many things about AWS that I have never done about it before. 
It's really fun, and convenient. For example, 
    + Cognito will support us in customer management, customer authentication, ... through self-developed services or 
    third parties such as Facebook, Google, ...
    + Amplify will support us to deploy web applications, mobile applications, ... very simple, fast and easy. It's only take
    time to set up the first time, and then it work very smooth and fast. 
- I learned how to parse a JWT string to Token? And verify it is valid or not? ...

#### Self assessment:
- I am confident that I have successfully completed the requirements set out in the assignment. Although there were some
difficulties when I first started out with AWS Cognito, how to auto deploy the web service from gitlab repo to amplify service,
Or some problems working with websites like _CORS error, ERR_SSL_PROTOCOL_ERROR, HTTPS site cannot call to api with HTTP, ..._ 
It really took a lot of my time to solve these problems. 
But it's really interesting, and worth the challenge. And I'm always looking for ways to solve each problem. 
Then, everything was ok. I can do it. 

- Besides, because of the above problems. And I have to go to work at the company so I only spend 2 days last weekend 
and 2 nights on Monday and Tuesday. So maybe I finish this exercise a bit slowly. 
- And maybe I always want to complete **this exercise as best I can, with the most seriousness and responsibility**, 
that's why I finish it a little late.

- I am not good at ReactJS, so web application I develop is only guaranteed in terms of requirements of assignment. 
I think it's acceptable and guaranteed to be requested, and I will read and learn to optimize and clean code better.

- **Improve and research**: 
    + I'm research how to deploy service (_backend Golang_) on AWS instead of Heroku or VPS. Because, I think AWS is a 
    huge ecosystem, and convenient for this. Because I don't have to much time to research for this assignment, so I will
    submit result to your company, and then I will continue research and optimize this problem. 


### Technologies stack: 
- AWS Service: 
    - [x] AWS Amplify - support deploy simple web application (ReactJS) via gitlab repository of project. 
    - [x] AWS Cognito - support manage user pool and authenticate request from user, return JWT token.
- Web Framework: 
    - [x] Gin-Gonic - support expose public RESTful API with 2 router `/internal` and `/healthcheck`
- Cloud Platform: 
    - [x] [VPS](https://cloud.google.com/compute) - Google Cloud - support deploy service by docker. To easy test and demo. 
    - [x] [Heroku](https://www.heroku.com/) - support deploy service on production env, and expose with [HTTPS domain](https://tranquil-scrubland-23383.herokuapp.com). 
- Web Library to build web application: 
    - [x] ReactJS (_bootstrap_)

### Check list requirement business: 
I have completed all requirement of the assignment. Detail below: 
- [x] **Develop done 100% requirement in the assignment**: 
    - [x] An API implemented as an HTTP server written in Go with 2 public API, and support verify JWT is valid?
    - [x] A small JavaScript web application, help user can login to the Cognito service. 
- [x] **Unit test coverage ~100%** ([detail at this segment](#unittest)):
    - [x] Unit test business logic when verify token
    - [x] Unit test in router of API

### Unit test/mock test report <a name="unittest"></a>:
- I have use some unit test/mock test library such as `testify`, `gomock` - support for testing business logic in this service. 
    + ![Test coverage](./docs/images/test_coverage.png)
- Detail report test coverage, you can read [at hear](./docs/coverage/coverage.html)

### Other report: 
- I have deployed backend service to 2 server (VPS and Heroku) to demo. And it also support for web application call to 
verify token. 
- Integrate CI with GitlabCI Runner on 2 repository (`login service - golang` and `web application`) to support run unit test,
lint code (`golangci-lint`) and build - compile service. It make sure that when have new commit code, the service is still
 up and running, and nothing goes wrong.
    + ![Test coverage](./docs/images/gitlab-ci-job.png)
- Organize source code clean (_develop, fix bug or write document on feature branch, then create MR to master_), and 
commit messages always follow a generic convention, are easy to review, maintain and can support the generate change log (if needed).
    + Gitlab Merge Request: 
        + ![Gitlab MR](./docs/images/gitlab-mr.png)
    + Tree Commit: 
        + ![Source Tree Commit](./docs/images/git-tree.png)

## Demo <a name="demo"></a>
### Web Service: 
- I had deployed web service on two environment to support for testing and demo. Detail: 
    + Service has been deployed on VPS (_GCP, Google Cloud - Linux (1 CPU, 4Gb RAM)_) 
        + Host Address: http://34.71.225.33:8080
        + How: I run with docker-compose.
    + Service has been deployed on Heroku: 
        + Host Address: https://tranquil-scrubland-23383.herokuapp.com
        + How: Trigger with repository. Build and deploy base on source code, branch master. 

### Web Application: 
- I had deployed web application on Amplify Service - AWS via source code Gitlab, branch master. 
    + Address: https://master.d1wo0to0xi8n6u.amplifyapp.com
- UI Page when login and authenticate JWT Token success: 
    + ![Source Tree Commit](./docs/images/ui-authen-success.png)
- UI Page when login success, but authenticate JWT Token fail: 
    + ![Source Tree Commit](./docs/images/ui-authen-fail.png)

## Reference: 
- [Github - How to Decode and verify Amazon Cognito JWT tokens](https://github.com/awslabs/aws-support-tools/tree/master/Cognito/decode-verify-jwt)
- [Document - How verifying a JSON Web Token](https://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-verifying-a-jwt.html#amazon-cognito-user-pools-using-tokens-step-1_)
---
### Thanks for reviewing my assignment

- Author: Vinh Huynh Anh
- Email: 
    + anhvinha1@gmail.com
    + vinhha.101296@gmail.com
- Phone: 039 604 4353
- Linkedin: https://www.linkedin.com/in/vinhha1012


