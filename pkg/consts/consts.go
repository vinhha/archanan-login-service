package consts

const (
	SigningMethodRS256 = "RS256"
	KeyID              = "kid"
)

const (
	SuccessCode         = 1
	FailCode            = -1
	HealthMessage       = "healthy"
	LoginSuccessMessage = "login successfully"
	ContentMessage      = "Welcome to Archanan Company"
)

const (
	LenAuthorizationToken = 2
	AuthorizationKey      = "Authorization"
)
