package errors

import "fmt"

var (
	ErrSigningMethodInvalid    = fmt.Errorf("SIGNING_METHOD_INVALID")
	ErrKeyNotFound             = fmt.Errorf("KEY_NOT_FOUND")
	ErrParseTokenError         = fmt.Errorf("PARSE_TOKEN_ERROR")
	ErrKidHeaderNotFound       = fmt.Errorf("KID_HEADER_NOT_FOUND")
	ErrTokenNotFound           = fmt.Errorf("TOKEN_NOT_FOUND")
	ErrTokenNotCorrectFormat   = fmt.Errorf("TOKEN_NOT_CORRECT_FORMAT")
	ErrTokenIsExpired          = fmt.Errorf("TOKEN_IS_EXPIRED")
	ErrValidationErrorAudience = fmt.Errorf("VALIDATION_ERROR_AUDIENCE")
)
