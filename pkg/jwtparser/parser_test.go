package jwtparser

import (
	"testing"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/stretchr/testify/assert"

	"archanan-login-service/pkg/errors"
)

func Test_parser_VerifyToken(t *testing.T) {
	tests := []struct {
		name         string
		tokenStr     string
		expectResult bool
		wantErr      bool
		errors       string
	}{
		{
			name:         "Test case 1 - Token is invalid algorithm",
			tokenStr:     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
			expectResult: false,
			wantErr:      true,
			errors:       errors.ErrSigningMethodInvalid.Error(),
		},
		{
			name:         "Test case 2 - Token is not exist KeyID",
			tokenStr:     "ewogICJhbGciOiAiUlMyNTYiCn0=.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA",
			expectResult: false,
			wantErr:      true,
			errors:       errors.ErrKidHeaderNotFound.Error(),
		},
		{
			name:         "Test case 3 - KeyID is not exist in array key",
			tokenStr:     "ewogICJraWQiOiAienA4K3JXZU4wZ1FldlppZ3F0R3FPeWFkVE5iLzhmNFJOTjl4T0ZvemlBPSIsCiAgImFsZyI6ICJSUzI1NiIKfQ==.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA",
			expectResult: false,
			wantErr:      true,
			errors:       errors.ErrKeyNotFound.Error(),
		},
		{
			name:         "Test case 4 - Parse token error",
			tokenStr:     "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBbKlF_FdtSA",
			expectResult: false,
			wantErr:      true,
			errors:       "illegal base64 data at input byte 341",
		},
		{
			name:         "Test case 5 - Parse token success, but token is expired",
			tokenStr:     "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA",
			expectResult: false,
			wantErr:      true,
			errors:       errors.ErrTokenIsExpired.Error(),
		},
		{
			name:         "Test case 6 - Parse token fail, with two segment of token",
			tokenStr:     "eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJldmVudF9pZCI6IjdlYTY0ODYzLTg4NzYtNGZmYy04ZDExLWUzODc4ZjczNDg5NSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTk4ODU0OTA4LCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0yLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMl9VZmxGcUFVbzEiLCJjb2duaXRvOnVzZXJuYW1lIjoiMjlmZDk4ZjctYjE4Ni00OGNiLThlMDYtYmZkNDVhNGQyYjEwIiwiZXhwIjoxNTk4ODU4NTA4LCJpYXQiOjE1OTg4NTQ5MDgsImVtYWlsIjoiYW5odmluaGExQGdtYWlsLmNvbSJ9.ZXBzfuMJGc71HBYwudZFFVM4YAuuQzs9K5ifjsAswlzsmYoYFg0Fpu6AbQMWRGiRrEljDH95hVLYhl47xFrsZZhu4-ANyUf4q6MrhCPW3ptrYqXrwCFPmv58Ah8o-mY6Xe-nnH7HlZO6itkeS5SQS5679DiayYoSvHI23yYj6_dNik7Jm0SnyYrUDcSlz14xpV6L7Nq8IDjUtxnPWoEX6idSfJZaM5lKgIK07z5s2Ly-fuzYymsYm1N_B3xNd8JLhyLCsKnq4O8hUtgDxm7l9ldvXI_AdcEc1X94P-lmQDqrBHW7AOUhhubfWZ1sgDa90w9UP6HI1PBJbKlF_FdtSA",
			expectResult: false,
			wantErr:      true,
			errors:       "token contains an invalid number of segments",
		},
		// This test case is not success every time, because token will be expired
		//{
		//	name:         "Test case 7 - Parse and verify successfully",
		//	tokenStr:     "eyJraWQiOiJ6cDgrcldlZE4wZ1FldlppZ3F0R3FPeWFkVE5iXC84ZjRSTk45eE9Gb3ppQT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIyOWZkOThmNy1iMTg2LTQ4Y2ItOGUwNi1iZmQ0NWE0ZDJiMTAiLCJhdWQiOiIxNWZjZnZsMDg0ZGN2cGxqYmNia3J2YzA5NCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU5ODk3MzI0OSwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMi5hbWF6b25hd3MuY29tXC91cy1lYXN0LTJfVWZsRnFBVW8xIiwiY29nbml0bzp1c2VybmFtZSI6IjI5ZmQ5OGY3LWIxODYtNDhjYi04ZTA2LWJmZDQ1YTRkMmIxMCIsImV4cCI6MTU5ODk3Njg0OSwiaWF0IjoxNTk4OTczMjUwLCJlbWFpbCI6ImFuaHZpbmhhMUBnbWFpbC5jb20ifQ.ZhejVrRlIqrmRGeJR5s7W-iIr6HmJBe6QEAi3q0MnVlQQg_QU1OSYO0bdDEYarY-fEjHu1se2li3ty-d_cjTNCrCiaxJ2mhy7b0uCqDFOouzIMZxVVdgC_DehXT5-an4MaDyNxmSJA29_wBEKAQ5q6-bq_yr4HOyeyr3R96HkMWfRJDQubOgCLPwFoVqkWLwujusWSt5Il-wjEAKrVATT6Ps2mYN_g4ZePaZqsDNw2MGBtrp0CKktiM5_5j27z2SzwI6sLeY85Sa5AM1CCnQC97e3KhAjP_rTZCy91oQIJfQpsicbvj8upvDCwznYB32SnaxJYRRnDQu2Zhxr9aRTw",
		//	expectResult: true,
		//	wantErr:      false,
		//	errors:       "",
		//},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Init parser instance:
			jwtKeyURL := "https://cognito-idp.us-east-2.amazonaws.com/us-east-2_UflFqAUo1/.well-known/jwks.json"
			keySet, err := jwk.Fetch(jwtKeyURL)
			p := &parser{
				jwtKeyURL: jwtKeyURL,
				keySet:    keySet,
			}

			// Call method verify token:
			output, err := p.VerifyToken(tt.tokenStr)

			// Assert result:
			if tt.wantErr {
				assert.Error(t, err)
				assert.Equal(t, tt.errors, err.Error())
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tt.expectResult, output)
			}
		})
	}
}
