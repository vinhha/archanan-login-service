package jwtparser

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/lestrrat-go/jwx/jwk"
	"github.com/sirupsen/logrus"

	"archanan-login-service/pkg/consts"
	"archanan-login-service/pkg/errors"
)

type Parser interface {
	VerifyToken(tokenStr string) (bool, error)
}

type parser struct {
	jwtKeyURL string
	keySet    *jwk.Set
}

func (p *parser) VerifyToken(tokenStr string) (bool, error) {
	// Call parse and verify token:
	_, err := p.parseAndVerifyToken(tokenStr)
	if err == nil {
		logrus.Infof("[Parser] VerifyToken - Token is valid and authenticate success")
		return true, nil
	}

	// If parse token return error, should check:
	// + If parse token error (ErrSigningMethodInvalid, ErrKeyNotFound, ErrKeyNotFound). Return error
	// + If verify token error, errors is a jwt.ValidationError. Should cast to service's error to easy handle.
	standardError := p.convertStandardError(err)
	logrus.Errorf("[Parser] VerifyToken - Token is invalid with error: %s", err.Error())
	return false, standardError
}

/*
	Func: support to parse a token string (jwt string from client) to a JWT Token (header.payload.sign). Then, verify token
	after parse success via library `github.com/lestrrat-go/jwx/jwk`
*/
func (p *parser) parseAndVerifyToken(tokenStr string) (*jwt.Token, error) {
	return jwt.Parse(tokenStr, p.parseTokenKeyFunc)
}

func (p *parser) parseTokenKeyFunc(token *jwt.Token) (interface{}, error) {
	if token.Method.Alg() != consts.SigningMethodRS256 {
		logrus.Errorf("unexpected signing method: %v", token.Header["alg"])
		return nil, errors.ErrSigningMethodInvalid
	}

	kid, ok := token.Header[consts.KeyID].(string)
	if !ok {
		logrus.WithFields(logrus.Fields{"header": token.Header}).Errorf("kid header not found")
		return nil, errors.ErrKidHeaderNotFound
	}

	keys := p.keySet.LookupKeyID(kid)
	if len(keys) == 0 {
		logrus.Errorf("key %v not found", kid)
		return nil, errors.ErrKeyNotFound
	}

	var raw interface{}
	err := keys[0].Raw(&raw)
	if err != nil {
		logrus.WithFields(logrus.Fields{"token": raw, "error": err}).Infof("parse token error")
		return nil, errors.ErrParseTokenError
	}

	logrus.WithFields(logrus.Fields{"token": raw}).Infof("parse token successfully")
	return raw, nil
}

// Func convert error: Token is valid, but should check token is expired, token used before issued, ...
// Validates time based claims "exp, iat, nbf".
// + Case "exp": return error jwt.ValidationErrorExpired
// + Case "iat": return error jwt.ValidationErrorIssuedAt
// + Case "nbf": return error jwt.ValidationErrorNotValidYet
// ...
func (p *parser) convertStandardError(err error) error {
	switch err.(*jwt.ValidationError).Errors {
	case jwt.ValidationErrorExpired:
		return errors.ErrTokenIsExpired
	case jwt.ValidationErrorAudience:
		return errors.ErrValidationErrorAudience
	}
	return err
}

func New(jwtKeyURL string) (Parser, error) {
	keySet, err := jwk.Fetch(jwtKeyURL)
	if err != nil {
		logrus.Errorf("fail to parse public key of AWS Cognito - User Pool")
		return nil, err
	}
	return &parser{
		jwtKeyURL: jwtKeyURL,
		keySet:    keySet,
	}, nil
}
