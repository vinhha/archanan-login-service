package router

type HealthCheckResponse struct {
	Status string `json:"status"`
}

type AuthenticateTokenResponse struct {
	ReturnCode    int32  `json:"return_code"`
	ReturnMessage string `json:"return_message"`
	DetailContent string `json:"detail_content"`
}
