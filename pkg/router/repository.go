package router

import (
	"context"

	"github.com/sirupsen/logrus"

	"archanan-login-service/pkg/jwtparser"
)

//go:generate mockgen -source=repository.go -destination=./gomock/repository.go
type Repository interface {
	AuthenticateTokenHandler(ctx context.Context, token string) (bool, error)
}

type repository struct {
	parser jwtparser.Parser
}

func (r *repository) AuthenticateTokenHandler(ctx context.Context, token string) (bool, error) {
	// Step 1: Call to parser to verify token
	isValid, err := r.parser.VerifyToken(token)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).Errorf("[Biz] Fail to parse and verify token")
		return false, err
	}

	// Step 2: Return result
	logrus.Infof("[Biz] Parse token and verify successfully")
	return isValid, nil
}

func NewRepository(parser jwtparser.Parser) Repository {
	return &repository{
		parser: parser,
	}
}
