package router

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"

	"archanan-login-service/pkg/consts"
	"archanan-login-service/pkg/errors"
)

type Service struct {
	repository Repository
}

func NewRouter(repository Repository) *Service {
	return &Service{
		repository: repository,
	}
}

func (s *Service) Register(g gin.IRouter) {
	g.GET("/healthcheck", s.heathCheck)
	g.POST("/internal", s.authenticateToken)
}

func (s *Service) heathCheck(ctx *gin.Context) {
	logrus.Info("[Router] return message and status http.StatusOK when call health check API")
	ctx.AbortWithStatusJSON(http.StatusOK, &HealthCheckResponse{
		Status: consts.HealthMessage,
	})
}

func (s *Service) authenticateToken(ctx *gin.Context) {
	// Step 1: Get token from header
	jwtToken, err := s.extractJWTToken(ctx)
	if err != nil {
		logrus.WithFields(logrus.Fields{"error": err}).
			Errorf("[Router] Get JWT Token from header of request with error")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, &AuthenticateTokenResponse{
			ReturnCode:    consts.FailCode,
			ReturnMessage: err.Error(),
		})
		return
	}

	// Step 2: Call verify token
	isValid, err := s.repository.AuthenticateTokenHandler(ctx, jwtToken)
	if err != nil {
		logrus.WithFields(logrus.Fields{"jwtToken": jwtToken, "error": err}).
			Errorf("[Router] Call authenticate JWT token in repository error")
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, &AuthenticateTokenResponse{
			ReturnCode:    consts.FailCode,
			ReturnMessage: err.Error(),
		})
		return
	}

	if !isValid {
		logrus.WithFields(logrus.Fields{"jwtToken": jwtToken}).
			Warn("[Router] JWT Token is invalid, can not authorization")
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	logrus.WithFields(logrus.Fields{"jwtToken": jwtToken}).
		Infof("[Router] Authenticate with JWT Token successfully")
	ctx.AbortWithStatusJSON(http.StatusOK, &AuthenticateTokenResponse{
		ReturnCode:    consts.SuccessCode,
		ReturnMessage: consts.LoginSuccessMessage,
		DetailContent: consts.ContentMessage,
	})
}

func (s *Service) extractJWTToken(ctx *gin.Context) (string, error) {
	jwtToken := ctx.GetHeader(consts.AuthorizationKey)
	if len(jwtToken) == 0 {
		return "", errors.ErrTokenNotFound
	}

	arrFieldOfToken := strings.Split(jwtToken, " ")
	if len(arrFieldOfToken) != consts.LenAuthorizationToken {
		return "", errors.ErrTokenNotCorrectFormat
	}

	return arrFieldOfToken[1], nil
}
