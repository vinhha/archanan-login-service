package configutils

import (
	"fmt"
	"log"
	"strings"

	"github.com/spf13/viper"
)

func LoadConfiguration(serviceName, configFile, configPath string) {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	log.SetPrefix(LocalConfigLogPrefix)
	logWarn("Loading configuration from local (file system or os env)")
	logInfo(fmt.Sprintf("configfile [%s] configPath [%s]", configFile, configPath))

	viper.SetConfigName(configFile)
	viper.AddConfigPath(configPath)
	viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", serviceName))
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		logError(fmt.Sprintln("Can't load configuration from local file system, error: ", err))
		return
	}
	logInfo(fmt.Sprintf("Using config file: %s", viper.ConfigFileUsed()))
	logInfo("Loaded configuration from local file system successfully")
}
